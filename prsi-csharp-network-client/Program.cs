﻿using System.Net;
using System.Text;
using Newtonsoft.Json;
using Websocket.Client;

Console.Clear();

const string dataPath = "data.conf";
const string lastIpPath = "last_ip";
const string lastNamePath = "last_name";

Dictionary<string, string> confData;

if (File.Exists(dataPath))
{
    try
    {
        confData = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(dataPath)) ??
                   new Dictionary<string, string>();
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
        confData = new Dictionary<string, string>();
    }
}
else
{
    confData = new Dictionary<string, string>();
}

var lastIp = "localhost:8080";

if (confData.ContainsKey(lastIpPath))
{
    lastIp = confData[lastIpPath];
}

Console.WriteLine($"Enter base url [{lastIp}]:");
var baseUrl = Console.ReadLine()??lastIp;

if(string.IsNullOrWhiteSpace(baseUrl)) {
    baseUrl = lastIp;
}

var lastName = "";

if (confData.ContainsKey(lastNamePath))
{
    lastName = confData[lastNamePath];
}

Console.WriteLine(string.IsNullOrWhiteSpace(lastName) ? "Enter your name:" : $"Enter your name [{lastName}]:");
var name = Console.ReadLine()??lastName;

if(string.IsNullOrWhiteSpace(name)) {
    name = lastName;
}

confData[lastIpPath] = baseUrl;
confData[lastNamePath] = name;

File.WriteAllText(dataPath, JsonConvert.SerializeObject(confData));
var httpClient = new HttpClient();

var userId = await httpClient
    .PostAsync($"https://{baseUrl}/user", new StringContent(name))
    .Result
    .Content
    .ReadAsStringAsync();

var options = new StringBuilder();
options.AppendLine("1) create session");
options.AppendLine("2) join session");

Console.WriteLine(options.ToString());

var option = int.Parse(Console.ReadLine()??"1");

string? playerSessionId = null;
string gameSessionId;

if(option == 1)
{
    var rawData = await httpClient
        .PostAsync($"https://{baseUrl}/session", new StringContent(userId)).Result.Content
        .ReadAsStringAsync();
    
    var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData)!;
    
    gameSessionId = data["session_id"];
    playerSessionId = data["player_session_id"];

    Console.WriteLine("Your created session id:");
    Console.WriteLine(gameSessionId);
} else {
    Console.WriteLine("Paste sessionId you wan to join \n: ");
    gameSessionId = Console.ReadLine()??"";
    var result = httpClient
        .PostAsync($"https://{baseUrl}/session/join/{gameSessionId}/{userId}", new StringContent(userId))
        .Result;

    if (result.StatusCode == HttpStatusCode.OK)
    {
        playerSessionId = await result.Content.ReadAsStringAsync();
    }
    else
    {
        Console.WriteLine("Something went wrong!");
        Environment.Exit(-1);
    }

    Console.WriteLine($"Joined session (ID: {gameSessionId})");
}

var webSocket = new WebsocketClient(new Uri($"wss://{baseUrl}/ws/{playerSessionId}"));
webSocket.MessageReceived.Subscribe(v =>
{
    if (v.Text.Equals("←[2J←[0;0H"))
    {
        Console.Clear();
    }
    else
    {
        Console.WriteLine(v.Text);
    }
});

webSocket.DisconnectionHappened.Subscribe(v => Environment.Exit(0));

new Thread(() => webSocket.Start()).Start();

if(option == 1) {
    Console.WriteLine("Press key to start game...");
    Console.Read();

    var response = await httpClient
        .PostAsync($"https://{baseUrl}/session/start/{gameSessionId}/{userId}", null)
        .Result
        .Content
        .ReadAsStringAsync();

    Console.WriteLine(response);
}

new Thread(() => CheckConsole(webSocket)).Start();

static void CheckConsole(IWebsocketClient webSocket) {
    while(true)
    {
        var message = Console.ReadLine();
        
        webSocket.Send(string.IsNullOrWhiteSpace(message)?"enter":message);
    }
}